# encoding: utf-8

#Imports
import random
import csv
import os
import time


########################################################################################################
#Defines
QUIT='quit'

FG_UNDERLINED='4'
FG_BLACK='30'
FG_RED='31'
FG_GREEN='32'
FG_YELLOW='33'
FG_BLUE='34'
FG_MAGENTA='35'
FG_CYAN='36'
FG_WHITE='37'
FG_GREY='90'

BG_DEFAULT='0'
BG_BLACK='40'
BG_RED='41'
BG_GREEN='42'
BG_YELLOW='43'
BG_BLUE='44'
BG_MAGENTA='45'
BG_CYAN='46'
BG_WHITE='47'

DEFAULT_STYLE='0'

MIN_PLAYERS=1
MAX_PLAYERS=4

HANGMAN={0 : '\
  +---+   \n\
  |   |   \n\
      |   \n\
      |   \n\
      |   \n\
      |   \n\
========= ',1 : '\
  +---+   \n\
  |   |   \n\
  O   |   \n\
      |   \n\
      |   \n\
      |   \n\
========= ',2 : '\
  +---+   \n\
  |   |   \n\
  O   |   \n\
  |   |   \n\
      |   \n\
      |   \n\
========= ',3 : '\
  +---+   \n\
  |   |   \n\
  O   |   \n\
 /|   |   \n\
      |   \n\
      |   \n\
========= ',4 : '\
  +---+   \n\
  |   |   \n\
  O   |   \n\
 /|\  |   \n\
      |   \n\
      |   \n\
========= ',5 : '\
  +---+   \n\
  |   |   \n\
  O   |   \n\
 /|\  |   \n\
 /    |   \n\
      |   \n\
========= ',6 : '\
  +---+   \n\
  |   |   \n\
  O   |   \n\
 /|\  |   \n\
 / \  |   \n\
      |   \n\
========= '}

STARTSTRING='\n\n\
\t\t\t\t   +---+   \n\
\t\t\t\t   |   |   \n\
\t\t\t\t   O   |   \n\
\t\t\t\t  /|\  |   \n\
\t\t\t\t  / \  |   \n\
\t\t\t\t       |   \n\
\t\t\t\t ========= \n\
==============================================================================\n\
||      ||    //\\\\    ||\\\\    || |||||||||| ||\\\\    //||    //\\\\    ||\\\\    ||\n\
||      ||   //  \\\\   || \\\\   || ||         || \\\\  // ||   //  \\\\   || \\\\   ||\n\
||||||||||  //====\\\\  ||  \\\\  || ||    |||| ||  \\\\//  ||  //====\\\\  ||  \\\\  ||\n\
||      || //      \\\\ ||   \\\\ || ||      || ||        || //      \\\\ ||   \\\\ ||\n\
||      ||//        \\\\||    \\\\|| |||||||||| ||        ||//        \\\\||    \\\\||\n\
==============================================================================\n\
\t\t\tAuthor: Miguel Berbegal Dec-2020'

GAMEOVERSTRING='\n\n\
  /     \                                        /     \\\n\
 | () () |                                      | () () |\n\
  \  ^  /  ___  __         __  __  _    _ __  __ \  ^  /\n\
   |||||  |  _ |__| |\_/| |_  |  |  \  / |_  |__| |||||\n\
   |||||  |___||  | |   | |__ |__|   \/  |__ |\_  |||||'

VICTORYSTRING='\n\n\
                                     ..::\'\'\'\'::..\n\
                                   .;''          ``;.\n\
                                  ::    ::  ::    ::\n\
_    _ _ __ ___  __   __  _   _  ::     ::  ::     ::\n\
 \  /  ||    |  |  | |__|  \_/   :: .:\' ::  :: `:. ::\n\
  \/   ||__  |  |__| |\     |    ::  :          :  ::\n\
                                  :: `:.      .:\' ::\n\
                                   `;..``::::\'\'..;\'\n\
                                      ``::,,,,::\'\''

WORDBANK=[]
with open('englishNouns.csv') as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        if len(row)>0:
            word=row[0]
            word=str.replace(word,'\ufeff','')
            word=str.replace(word,'ï»¿','')
            WORDBANK.append(word)


 

########################################################################################################
#Functions
def printc(message, fgColor, bgColor=BG_DEFAULT, endc='\n'):
    if bgColor==BG_DEFAULT:
        print(f'\x1b[{fgColor}m{message}\x1b[0m', end=endc)
    else:
        print(f'\x1b[{fgColor};{bgColor}m{message}\x1b[0m', end=endc)

def playerStr(playerNo):
    colorCode=f'3{playerNo+3}'
    if colorCode=='37':#change white for grey for the fourth player
        return f'\x1b[90mPlayer {playerNo}\x1b[0m'
    else:
        return f'\x1b[3{playerNo+3}mPlayer {playerNo}\x1b[0m'

def inputWithQuit(message):
    ui=input(message)
    if ui.lower()==QUIT:
        exit()
    else:
        return ui

def isDigitBetweenRange(testArgument, startRange, endRange):
    if testArgument.isdigit():
        testArgumentInt=int(testArgument)
        if testArgumentInt>=startRange and testArgumentInt<=endRange:
            return True #the string is an integer between the selected range
        else:
            return False #the string is an integer but is not in the selected range
    else:
        return False #the string is not a number       

def findAllsubstringsInString(substring, string):
    iter=-1
    matches=[]
    while string.find(substring, iter+1)!=-1:
        iter=string.find(substring, iter+1)
        matches.append(iter)
    return matches

def createDashedWord(string):
    length=len(string)
    spaces=''
    for i in range(length):
        spaces=spaces+'_ '
    return spaces

def createPartialDashedWord(secretWord, dashed, newLetter):
    matches=findAllsubstringsInString(newLetter, secretWord)
    stringCopy=dashed
    dashed=''
    for i in range(len(stringCopy)):
        if i/2 in matches:
            dashed=dashed+newLetter
        elif stringCopy[i]!='_':
            dashed=dashed+stringCopy[i]
        else:
            dashed=dashed+'_'
    return dashed

def getLetterFromPlayer(player):
    letter=inputWithQuit(f'\n{playerStr(player)} it\'s your turn, please type a letter:_')
    while not letter.isalpha() or len(letter)!=1:
        letter=inputWithQuit('You can only type one letter of the alphabet:_')
    letter=letter.lower()
    return letter

def printGameStatus(lifes, hangman, nFails, spaces, wrongLetters):
    #clear screen
    time.sleep(1)
    if os.name=='posix': #UNIX (Linux, Mac)
        os.system('clear')
    elif os.name=='nt': #Windows
        os.system('cls')
    else:#unkown system
        pass 

    #print game screen
    for key in lifes:
        print(f'{playerStr(key)}: {lifes[key]} lifes left')
    print(f'Wrong letters: {wrongLetters}')
    print(hangman[nFails], end=' ')
    print(f'the word is: {spaces}')

def nextPlayer(currentPlayer, nPlayers, lives):
    nextPlayer=currentPlayer+1
    if nextPlayer>nPlayers:#circle back to first
        nextPlayer=1
    while lives[nextPlayer]==0 and nextPlayer!=currentPlayer:#skip dead players
        nextPlayer+=1
        if nextPlayer>nPlayers:
            nextPlayer=1
    return nextPlayer

def printSessionScores(nPlayers, wins, matches):
    printc('\nSession scores:',FG_UNDERLINED)
    maxWins=0
    for i in range(nPlayers):
        if wins[i+1]>maxWins:
            maxWins=wins[i+1]
    for i in range(nPlayers):
        if wins[i+1]==maxWins and maxWins!=0:
            print(f'{playerStr(i+1)} won {wins[i+1]} out of {matches} matches', end=' ')
            printc('(1st)',FG_YELLOW)
        else:
            print(f'{playerStr(i+1)} won {wins[i+1]} out of {matches} matches')
        
def hangmanGame(nPlayers):
    #Session variables
    nPlayers=int(nPlayers)
    maxLives=int(len(HANGMAN)/nPlayers+0.5)
    endGame=False
    wins={}
    matches=0
    for i in range(nPlayers):
        wins[i+1] = 0
    while not endGame:
        print(f'Preparing game for {nPlayers} players...')
        #Match variables
        currentPlayer=1
        if nPlayers>1:
            currentPlayer=random.randrange(1,nPlayers+1,1)
        deadPlayers=[]
        nFails=0
        correctLetters=[]
        wrongLetters=[]
        lifes={}
        for i in range(nPlayers):
            lifes[i+1] = maxLives
        secretWord=random.choice(WORDBANK)
        dashes=createDashedWord(secretWord)

        print('Let\'s start!\n')
        printGameStatus(lifes,HANGMAN,nFails,dashes, wrongLetters)

        while not endGame:
            letter=getLetterFromPlayer(currentPlayer)
            if letter in secretWord and letter not in correctLetters:
                printc('Correct!\n',FG_GREEN)
                correctLetters.append(letter)
                dashes=createPartialDashedWord(secretWord, dashes,letter)
                printGameStatus(lifes,HANGMAN,nFails,dashes, wrongLetters)
                if '_' not in dashes:
                    wins[currentPlayer]+=1
                    printc(f'\nCONGRATULATIONS {playerStr(currentPlayer)}',FG_GREEN, endc='')
                    printc(f'! YOU WON!',FG_GREEN)
                    printc(VICTORYSTRING,FG_YELLOW)
                    endGame=True
            else:
                printc('Wrong!',FG_RED)
                lifes[currentPlayer]-=1
                nFails+=1
                if letter not in correctLetters and letter not in wrongLetters:
                    wrongLetters.append(letter)
                if lifes[currentPlayer]==0:
                    printc(f'Player {currentPlayer} you died... :(\n',FG_RED)
                    deadPlayers.append(currentPlayer)
                else:
                    print('\n')
                printGameStatus(lifes,HANGMAN,nFails,dashes, wrongLetters)

                if nFails==len(HANGMAN)-1 or len(deadPlayers)==nPlayers:
                    print(f'\nUps... You didn\'t guess the word... the secret word was:', end=' ')
                    printc(secretWord,FG_UNDERLINED)
                    printc(GAMEOVERSTRING, FG_RED)
                    endGame=True
                if not endGame:
                    currentPlayer=nextPlayer(currentPlayer, nPlayers, lifes)

        matches+=1            
        printSessionScores(nPlayers,wins,matches)
        inputWithQuit('\nDo you want a rematch? Press enter to play again, type \'quit\' to quit:_')
        endGame=False
    return True

########################################################################################################
def main():
    printc(STARTSTRING,FG_YELLOW)


    inputWithQuit('\nWelcome to hangman! Press enter to Start, type \'quit\' to quit now or at any point of the game:_')
    nPlayers=inputWithQuit('How many players will be playing?:_')
    while not isDigitBetweenRange(nPlayers,MIN_PLAYERS,MAX_PLAYERS):
        nPlayers=inputWithQuit('Sorry, didn\'t understand you. The number of players must be between 1 and 4:_')

    hangmanGame(nPlayers)

if __name__ == "__main__":
    main()





        








